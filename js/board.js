class Board{
	constructor(boardWidth, boardHeight){
		this.height = boardHeight;
		this.width = boardWidth;
		this.grid = [];
		for(var y = 0;y<boardHeight;y++){
			this.grid.push(new Array(boardWidth).fill(0));
		}
	}

	draw(){
		this.grid.forEach((rows, y) => {
			rows.forEach((p, x)  => {
				if(p !== 0){
					stroke(p);
					point(x,y);
				}
			})
		})
	}

	removeFullLines(){

		this.grid.forEach( (row, key) => {
			var sum = 0;
			row.forEach( (row, x) => {
				if(row !== 0){
					sum++;
				}
			})
			if(sum == this.width){
				this.removeSingleLine(key);
			}
		});
		
	}

	removeSingleLine(idx){
		this.grid.splice(idx,1);				// Removing the filled line
		this.grid.unshift(new Array().fill(0));	// Prepending a new, empty line
	}
}