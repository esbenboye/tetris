class RandomPicker{
	constructor(objects){
	  this.objects = objects.slice();
    this.original = objects.slice();
  }
  
  getElement(){
  	if(this.objects.length == 0){
    	this.objects = this.original.slice();
    }
  
  	var rand = Math.floor(random(this.objects.length));
    return this.objects.splice(rand,1)[0];
  }
}