class Tile{

	canMove(x,y){
  	
    // This magically knows about the board

    var newPos = {
      x:this.pos.x + x,
      y:this.pos.y + y
    }
    var ret = true;
    tile.tile.forEach( (rows, y) => {
      rows.forEach( (px, x) => {

          if(px == 0){
            return;            
          }

          if(board.grid[y + newPos.y] == undefined){
            console.log("Too far up or down");
            ret = false;
            return;
          }

          if(board.grid[y + newPos.y][x+newPos.x] == undefined && px == 1){
            console.log("Too far left or right");
            ret = false;
            return;
          }

          if(board.grid[y + newPos.y] === 1){
            console.log("Space taken up or down");
            ret = false;
            return;
          }

          if(board.grid[y + newPos.y][x+newPos.x] !== 0){
            console.log("Space taken left or right");
            ret =  false;
            return;
          }
        })
    })
    return ret;
  }

	constructor(color){

  	this.tile = [];
  	this.pos = {};
  	this.pos.x = 1;
	  this.pos.y = 1;
    this.color = color;
  }
	
  fall(){
  	var res = this.move(0,1);
    return res;
  }
  
  move(x,y){	
    if(this.canMove(x,y) === false){
      console.log("NOT MOVING");
    	return false;
    }
  	this.pos.x+=x;
    this.pos.y+=y;
    return true;
  }
  
	rotate(dir){
    for(var y = 0;y < this.tile.length; y++){
      for(var x = 0;x< y; x++){
        // Switching xy positions
        [this.tile[x][y], this.tile[y][x]] = [this.tile[y][x],this.tile[x][y]];
      }
    }
    if(dir == 1){
      this.tile.forEach(row=>row.reverse());  
    }else{
      this.tile.reverse();
    } 
  }
  
  draw(){
  	push();
    stroke(this.color);
  	this.tile.forEach( (rows, yIndex) => {
    	rows.forEach( (cols, xIndex) => {
				if(cols){
        	point(xIndex + this.pos.x, yIndex + this.pos.y);
        }
      })
    })
    pop();
  } 

  getColor(){
    return this.color;
  }
}