var board;
var tile;
var scl = 20;

var w = 12;
var h = 20;

var timeDelta = 1000;
var lastTime = 0;

var rp;

/* P5 functions */

function setup(){
	board = new Board(w,h);
	setupRandomColorPicker();
	createCanvas(w*scl, h*scl);
	tile = getRandomTile();
	stroke(255,0,0);
}

function draw(){

	scale(scl);
	background(51);

	if(millis()-lastTime > timeDelta){
		lastTime = millis();
		var res = tile.fall();
		if(res === false){
			lockTileToTable();
			tile = getRandomTile();
			tile.pos.y = 1;
			tile.pos.x = 5;
		}
		board.removeFullLines();
	}
	
	tile.draw();
	board.draw();
}


function keyPressed(){

	switch(keyCode){
		case 39:
			tile.move(1,0);
		break;
		case 37:
			tile.move(-1,0);
		break;
		case 65:
			tile.rotate(1);
		break;
		case 83:
			tile.rotate(-1);
		break;
	}
	

	
}

/* Helper functions */

function setupRandomColorPicker(){
	rp = new RandomPicker(
	[
      color(255,26,0),
      color(0,140,0),
      color(64,150,238),
      color(255,0,132),
      color(255,255,136)
    ]
);
}

function lockTileToTable(){
	tile.tile.forEach( (rows, y) => {
  	rows.forEach( (px, x) => {
		if(px){
      		board.grid[y + tile.pos.y][x + tile.pos.x] = tile.getColor();
      	}
    })
  })
}

function getRandomTile(){
	let x = Math.round(random(1,5));
	var color = rp.getElement();

	if(x == 1){
		return new lTile(color);
	}else if(x == 2){
		return new oTile(color);
	}else if(x == 3){
		return new sTile(color);
	}else if(x == 4){
		return new tTile(color);
	}else if(x == 5){
		return new zTile(color);
	}
}


Array.prototype.sum = function(){
	let sum = 0;
  this.forEach((value) => {
  	sum+=value;
  })
  return sum;
}
